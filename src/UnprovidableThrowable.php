<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DataProvider;

use Throwable;

/**
 * UnprovidableThrowable interface file.
 * 
 * Unprovidable Throwables are thrown then the data is unreachable for any
 * reason, either its source is unavailable, or it can't be read, or it can't
 * be decoded, decrypted, unmarshalled, etc.
 * 
 * @author Anastaszor
 */
interface UnprovidableThrowable extends Throwable
{
	
	/**
	 * Gets the source of the data provider that failed.
	 *
	 * @return string
	 */
	public function getSource() : string;
	
	/**
	 * Gets the index at which the providing failed.
	 * 
	 * @return integer
	 */
	public function getIndex() : int;
	
}
