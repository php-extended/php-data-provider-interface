<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DataProvider;

use Iterator;
use Stringable;

/**
 * DataProviderInterface interface file.
 * 
 * This interface specifies how to provide raw data in form of arrays of
 * something.
 * 
 * @author Anastaszor
 */
interface DataProviderInterface extends Stringable
{
	
	/**
	 * Gets the source of the data provider. The source is an human-readable
	 * string that represents some path to the dataset we're trying to read,
	 * like a filesystem path, or an uri.
	 * 
	 * @return string
	 */
	public function getSource() : string;
	
	/**
	 * Gets whether this data provider has an unique data point. The data point
	 * is unique when a single object is at the top of the data hierarchy. This
	 * correspond to traditional information stored into tree-like structures,
	 * like json, xml, html, etc.
	 * 
	 * If this is false, it means that there are no single data points, so the
	 * underlying document gives a list of data points. This correspond to
	 * traditional information stored into array-like structures, like csv,
	 * json-multiline, etc.
	 * 
	 * If this is true, it is recommanded to use the provideOne() method of this
	 * class, and if this is false, to use the provideAll() (assuming everything
	 * can go in-memory) or provideIterator() (assuming everything cannot go
	 * in-memory) methods of this class.
	 * 
	 * @return boolean
	 */
	public function hasUnique() : bool;
	
	/**
	 * Provides an unique data point. This correspond to a single structure of
	 * array_recursive<integer|string, null|boolean|integer|float|string>,
	 * where the recursive part means that an array_recursive of the same type
	 * may appear into the value part of the array.
	 * 
	 * We can define :
	 * array_recursive<K, T> === array<K, T|array_recursive<K, T>>
	 * 
	 * This throws an UnprovidableThrowable if any error occur within the
	 * internal provider at the time of data retrieval.
	 * 
	 * If hasUnique() is false, this will return only the first entry of the
	 * underlying dataset. This method cannot be used in a while-next-loop, you
	 * must use the provideIterator() for this usage.
	 * 
	 * @return array<integer|string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string>>
	 * @throws UnprovidableThrowable
	 */
	public function provideOne() : array;
	
	/**
	 * Provides all the data points in one shot. Do not call this method if
	 * you cannot predict the size of the dataset you're providing data from,
	 * as this method will throw an OutOfMemoryError if the dataset you're
	 * loading is bigger than the available memory.
	 * 
	 * This throws an UnprovidableThrowable if any error occur within the
	 * internal provider at the time of data retrieval.
	 * 
	 * If hasUnique() is true, an array with an unique entry will be returned.
	 * 
	 * @return array<integer, array<integer|string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string>>>
	 * @throws UnprovidableThrowable
	 */
	public function provideAll() : array;
	
	/**
	 * Provides all the data points in an Iterator. The iterator throws
	 * UnprovidableThrowables when data cannot be iterated over, or at build
	 * time if the resource is unavailable.
	 * 
	 * If hasUnique() is true, an Iterator with an unique entry will be returned.
	 * 
	 * @return Iterator<integer, array<integer|string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string>>>
	 * @throws UnprovidableThrowable
	 */
	public function provideIterator() : Iterator;
	
}
